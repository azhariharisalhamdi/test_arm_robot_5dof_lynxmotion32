#include <stdio.h>
#include <time.h>

#include <wiringPi.h>

#define PORT_ADC1   0


int main (int argc, char *argv[])
{
    static int timer = 0 ;
  int adcValue;

    wiringPiSetup();

    for(;;)    {  

        if (millis () < timer)  continue ;

        timer = millis() + 100;

        adcValue = analogRead(PORT_ADC1);

       printf("Value read is %d\n", adcValue);
    }

    return 0 ;
}
